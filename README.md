# Asclepios-Server

SSE server is a main component for SSE. It stores the encrypted data which is uploaded by the SSE client. Besides, SSE server also provides the services such as search/update/delete over the store data. 

The details about how to run SSE server in docker container could be found in [SSE_deployment_manual.pdf](https://gitlab.com/asclepios-project/ssemanual/-/blob/develop/SSE_deployment_manual.pdf). Apart from that, the details about how to run SSE server using MiCADO could also be reached in [SSE_deployment_manual.pdf](https://gitlab.com/asclepios-project/ssemanual/-/blob/develop/SSE_deployment_manual.pdf).
